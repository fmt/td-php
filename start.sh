#!/bin/bash

if [ $1 ]
then
    php index.php $1
else
    echo "Please add the input number list filename to this bash script"
fi
