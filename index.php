<?php
$regexp = '/^(\+|00)?[ ]?((\d{3})|(\d{7,12}))$/';
$numArray = array();
$totals = array();

$fileUpload = $_SERVER['argv'][1];
$fileContent = file_get_contents($fileUpload, FILE_USE_INCLUDE_PATH);
$numArray = preg_split('/\r?\n/', $fileContent);

function phoneTest($arrayItem) {
    global $regexp;
    $arrayItem = trim($arrayItem);

    if(preg_match($regexp, $arrayItem)) {
        return $arrayItem;
    }
}
function arrayFilter($array) {
    if(!empty($array)) {
        return array_filter($array);
    }
}

$numArray = array_map(phoneTest, $numArray);
$numArray = arrayFilter($numArray);

$areaCodes = file_get_contents('./public/areacodes', FILE_USE_INCLUDE_PATH);
$areaCodes = preg_split('/\r?\n/', $areaCodes);

foreach($numArray as $phone) {
    if(strlen($phone) < 6) {
        continue;
    }
    $tmp = preg_replace('/^(\+|00)/', '', $phone);
    $tmp = substr($tmp, 0, 3);

    if(!in_array($tmp, $areaCodes)) {
        $tmp = substr($tmp, 0, 2);
    }
    if(!in_array($tmp, $areaCodes)) {
        $tmp = substr($tmp, 0, 1);
    }
    if(!in_array($tmp, $areaCodes)) {
        continue;
    }
    if(!$totals[$tmp]) {
        $totals[$tmp] = 0;
    }
    $totalTmp = $totals[$tmp];
    $totals[$tmp] = $totalTmp+1;
}
ksort($totals);

foreach($totals as $k => $v) {
    echo $k . ': ' . $v . "\n";
}
